package dev.team3.webapp.models;

import dev.team3.orm.annotations.ObjectKey;
import dev.team3.orm.annotations.PrimaryKey;
import dev.team3.orm.annotations.Table;

import java.util.Objects;

@Table(name = "reviews")
public class Review {
    @PrimaryKey
    @ObjectKey(name = "user_id")
    private int userId;
    @PrimaryKey
    @ObjectKey(name = "game_id")
    private int gameId;
    @ObjectKey(name = "review")
    private String review;

    public Review(){ super(); }

    public Review(String review, int gameId, int userId){
        super();
        this.gameId = gameId;
        this.review = review;
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Review review1 = (Review) o;
        return userId == review1.userId && gameId == review1.gameId && review.equals(review1.review);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, gameId, review);
    }

    @Override
    public String toString() {
        return "* Review: " + review + "\n";
    }
}
