package dev.team3.webapp.models;


import dev.team3.orm.annotations.IsDbGenerated;
import dev.team3.orm.annotations.ObjectKey;
import dev.team3.orm.annotations.PrimaryKey;
import dev.team3.orm.annotations.Table;

import java.util.List;
import java.util.Objects;

@Table(name = "games")
public class Game
{
    @PrimaryKey
    @IsDbGenerated
    @ObjectKey(name = "game_id")
    private int id;
    @ObjectKey(name = "game_name")
    private String name;

    private List<Review> reviews;

    public Game(){ super() ; }

    public Game(String name){
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGameName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return id == game.id && name.equals(game.name) && Objects.equals(reviews, game.reviews);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, reviews);
    }

    @Override
    public String toString() {
        return "* " +
                name + "\n";
    }
}
