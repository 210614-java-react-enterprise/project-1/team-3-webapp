package dev.team3.webapp.models;


import dev.team3.orm.annotations.IsDbGenerated;
import dev.team3.orm.annotations.ObjectKey;
import dev.team3.orm.annotations.PrimaryKey;
import dev.team3.orm.annotations.Table;

import java.util.Objects;

@Table(name="users")
public class User
{
    @PrimaryKey
    @IsDbGenerated
    @ObjectKey(name="user_id")
    private int id;
    @ObjectKey(name="admin")
    boolean admin;
    @ObjectKey(name="username")
    private String username;
    @ObjectKey(name="password")
    private String password;

    public User()
    {
        admin = false;
    }
    public User(String username, String password, boolean isAdmin){
        super();
        this.username = username;
        this.password = password;
        this.admin = isAdmin;
    }
    public User(int id, String username, String password, boolean isAdmin)
    {
        this.id = id;
        this.username = username;
        this.password = password;
        this.admin = isAdmin;
    }


    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id && username.equals(user.username) && password.equals(user.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, password);
    }

    @Override
    public String toString() {
        return "* "+ username + "\n" +
                "Admin: "+ isAdmin()+"\n";
    }
}
