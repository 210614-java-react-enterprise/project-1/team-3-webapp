package dev.team3.webapp.Servlets;

import dev.team3.webapp.models.User;

import dev.team3.webapp.services.AuthService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

public class LoginServlet extends HttpServlet {

    private AuthService as = new AuthService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String usernameParam = req.getParameter("username");
        String passwordParam = req.getParameter("password");
        User user = as.getPermissions(usernameParam, passwordParam);
        if(user!=null){
            HttpSession s = req.getSession();
            s.setAttribute("loggedIn", true);
            s.setAttribute("username", usernameParam);
            s.setAttribute("admin", user.isAdmin());
            try(PrintWriter pw = resp.getWriter()){
                resp.setHeader("Content-Type", "text/html;charset=utf-8");
                pw.write("Welcome, "+usernameParam+"!");
            }
        } else {
            resp.sendError(401, "Invalid credentials");
        }
    }


}
