package dev.team3.webapp.Servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.team3.webapp.models.Game;
import dev.team3.webapp.models.Review;
import dev.team3.webapp.services.ReviewService;
import dev.team3.webapp.services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.annotation.Retention;
import java.util.ArrayList;
import java.util.List;

public class ReviewServlet extends HttpServlet {

    final ReviewService reviewService = new ReviewService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{

        String userIdContainsQueryParam = req.getParameter("user_id");
        String gameIdContainsQueryParam = req.getParameter("game_id");


        if(userIdContainsQueryParam != null && gameIdContainsQueryParam != null){
            Review review = reviewService.getOneReviewOfUser(Integer.parseInt(userIdContainsQueryParam), Integer.parseInt(gameIdContainsQueryParam));
            PrintWriter pw = resp.getWriter();
            pw.write(review.toString());

        }else if(userIdContainsQueryParam != null){
            List<Review> reviews = reviewService.getReviewsOfUser(Integer.parseInt(userIdContainsQueryParam));
            for(Review g : reviews){
                PrintWriter pw = resp.getWriter();
                pw.write(g.toString());
            }
        }else if(gameIdContainsQueryParam != null){
            List<Review> reviews = reviewService.getReviewsOfGame(Integer.parseInt(gameIdContainsQueryParam));
            for(Review g : reviews){
                PrintWriter pw = resp.getWriter();
                pw.write(g.toString());
            }
        }
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Review review = null;
        try(BufferedReader reader = req.getReader()) {
            String reviewJson = reader.readLine();

            ObjectMapper objectMapper = new ObjectMapper();
            review = objectMapper.readValue(reviewJson, Review.class);
        }
        if(reviewService.createReview(review)){
            resp.setStatus(201);
            try(PrintWriter pw = resp.getWriter()){
                pw.write("Success!");
            }
        }else{
            resp.sendError(400, "There was a problem creating review!");
        }
    }
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        if(session == null){
            resp.sendError(401, "No active session, please log in.");
        }
        String newReview = null;

        try(BufferedReader reader = req.getReader()) {
            newReview = reader.readLine();
        }

        String userId = req.getParameter("user_id");
        String gameId = req.getParameter("game_id");
        String username = req.getParameter("username");

        Review review = null;

        if(userId != null && gameId != null && newReview != null){
            review = reviewService.getOneReviewOfUser(Integer.parseInt(userId), Integer.parseInt(gameId));
            review.setReview(newReview);
        }else{
            resp.sendError(500, "Please specify which review to update.");
        }

        if (session != null) {
            if (username.equals(req.getParameter("username"))) {
                if (reviewService.updateReview(review)) {
                    resp.setStatus(200);
                    try (PrintWriter pw = resp.getWriter()) {
                        pw.write("Review was Updated successfully!");
                    }
                } else {
                    resp.sendError(500, "There was an error updating your review");
                }
            }
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        String column = req.getParameter("isUserReview");
        String username = req.getParameter("username");

        if (column.toLowerCase().equals("yes")) {
            column = "user_id";
        } else if (column.toLowerCase().equals("no")) {
            column = "game_id";
        } else {
            resp.sendError(401, "Please select yes or no for column.");
        }

        HttpSession session = req.getSession(false);
        if (session == null) {
            resp.sendError(401, "No active session, please log in.");
        }

        if (session != null && (boolean) session.getAttribute("admin")) {
            if (reviewService.deleteReview(Integer.parseInt(id), column)) {
                resp.setStatus(200);
                try (PrintWriter pw = resp.getWriter()) {
                    pw.write("Review was Deleted successfully!");
                }
            } else {
                resp.sendError(500, "There was an error Deleting review.");
            }
        } else if (session != null && session.getAttribute("username").equals(username)) {
            if (reviewService.deleteReview(Integer.parseInt(id), column)) {
                resp.setStatus(200);
                try (PrintWriter pw = resp.getWriter()) {
                    pw.write("Review was Deleted successfully!");
                }
            } else {
                resp.sendError(500, "There was an error Deleting review.");
            }
        }
    }
}
