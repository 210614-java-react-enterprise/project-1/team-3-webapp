package dev.team3.webapp.Servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.team3.webapp.models.Game;
import dev.team3.webapp.models.User;
import dev.team3.webapp.services.GameService;
import dev.team3.webapp.services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class UserServlet extends HttpServlet {

    private UserService userService = new UserService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{

        HttpSession session = req.getSession(false);
        if(session == null){
            resp.sendError(401, "No active session, please log in.");
        }

        String user_id = req.getParameter("user_id");

        User user = null;

        user = userService.getUser(Integer.parseInt(user_id));

        if(user != null){

            ObjectMapper objectMapper = new ObjectMapper();
            String json = objectMapper.writeValueAsString(user);

            try(PrintWriter pw = resp.getWriter()){
                pw.write(user.toString());
            }
        }else {
            resp.sendError(500, "There was an error getting user.");
        }

    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession(false);
        if(session == null){
            resp.sendError(401, "No active session, please log in.");
        }

        String updateUsernameParam = "";
        String updatePasswordParam = "";
        String idParam = req.getParameter("user_id");

        if(req.getParameter("update-username") != null){
            updateUsernameParam = req.getParameter("update-username");
        }

        if(req.getParameter("update-password") != null){
            updatePasswordParam = req.getParameter("update-password");
        }

        if(userService.updateUser(updateUsernameParam, updatePasswordParam, Integer.parseInt(idParam))){
            resp.setStatus(201);
            try(PrintWriter pw = resp.getWriter()){
                pw.write("User updated!");
            }
        }else
            resp.sendError(500, "There was an error updating user");

    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userToDelete = req.getParameter("userID");
        String username = req.getParameter("username");

        HttpSession session = req.getSession(false);
        if(session == null){
            resp.sendError(401, "No active session, please log in.");
        }


        if (session != null && (boolean)session.getAttribute("admin")) {
            if(userService.deleteUser(Integer.parseInt(userToDelete))){
                resp.setStatus(200);
                try(PrintWriter pw = resp.getWriter()){
                    pw.write("User was deleted.");
                }
            }else {
                resp.sendError(500, "There was an error deleting user.");
            }

        } else if(session != null && req.getAttribute("username").equals(username)){
            if(userService.deleteUser(Integer.parseInt(userToDelete))){
                resp.setStatus(200);
                try(PrintWriter pw = resp.getWriter()){
                    pw.write("User was deleted.");
                }
            }else {
                resp.sendError( 500, "There was an error deleting user.");
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}
