package dev.team3.webapp.Servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.team3.webapp.models.Game;
import dev.team3.webapp.services.GameService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class GameServlet extends HttpServlet {

    private GameService gameService = new GameService();

    public GameServlet(){}

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
        HttpSession session = req.getSession(false);
        if(session == null){
            resp.sendError(401, "No active session, please log in.");
        }

        List<Game> games;
        String nameContainsQueryParam = req.getParameter("name-contains");
        if(nameContainsQueryParam != null){
            games = gameService.getGamesByName(Game.class, nameContainsQueryParam);
        }else{
            if(session != null && session.getAttribute("Game-list")==null){
                games = gameService.getAllGames(Game.class);
                session.setAttribute("Game-list", games);
                System.out.println("Adding game list to cache");
            } else {
                games = (List<Game>)session.getAttribute("Game-list");
            }
        }
        for(Game g : games){
            PrintWriter pw = resp.getWriter();
            pw.write(g.toString());
        }
    }



    @Override//Create
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
        HttpSession session = req.getSession(false);
        if(session == null){
            resp.sendError(401, "No active session, please log in.");
        }

        Game game = null;
        try(BufferedReader reader = req.getReader()) {
            String gameJson = reader.readLine();

            ObjectMapper objectMapper = new ObjectMapper();
            game = objectMapper.readValue(gameJson, Game.class);
        }

        if(gameService.createGame(game)){
            resp.setStatus(201);
            try(PrintWriter pw = resp.getWriter()){
                pw.write("Success!");
            }
        }

    }

    @Override//delete
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
        String gameToDelete = req.getParameter("game_id");

        HttpSession session = req.getSession(false);
        if(session == null || (boolean)session.getAttribute("admin") == false){
            resp.sendError(401, "No active session, please log in.");
        } else {
            boolean success = gameService.deleteGame(Integer.parseInt(gameToDelete));
            if(success){
                resp.setStatus(200);
                try(PrintWriter pw = resp.getWriter()){
                    pw.write("Game was deleted");
                }
            }else {
                resp.sendError(400, "There was an error deleting Game");
            }

        }

    }


}
