package dev.team3.webapp.services;

//import dev.team3.orm.orm.OrmImpl;
import dev.team3.orm.orm.OrmImpl;
import dev.team3.webapp.models.Review;
import dev.team3.webapp.models.User;

import java.util.ArrayList;
import java.util.List;

public class UserService {

    private OrmImpl orm = new OrmImpl();

    public User getUser(int id){
        List<User> users = new ArrayList<>();
        for(Object ob: OrmImpl.getAll(User.class)){
            users.add((User)ob);
        }

        return users.stream()
                .filter(User -> User.getId() == id)
                .findFirst().orElse(null);
    }

    public User createUser(User user){
        User success = orm.create(user);
        return success;
    }

    public boolean updateUser(String username, String password, int id){
        User user = getUser(id);
        user.setUsername(username);
        user.setPassword(password);
        OrmImpl.update(user);
        return true;
    }

    public boolean deleteUser(int user_id){
       // OrmImpl.delete(Review.class, user_id, "user_id");
        return OrmImpl.delete(User.class, user_id, "user_id");
    }
}
