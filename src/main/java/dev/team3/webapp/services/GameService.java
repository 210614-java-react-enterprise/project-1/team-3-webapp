package dev.team3.webapp.services;


import dev.team3.orm.orm.OrmImpl;
import dev.team3.webapp.models.Game;
import dev.team3.webapp.models.Review;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class GameService {

    OrmImpl orm = new OrmImpl();

    public List<Game> getAllGames(Class<?> clazz){
        List<Game> games = new ArrayList<>();
        for (Object ob: OrmImpl.getAll(Game.class)) {
            Game game = (Game)ob;
            ReviewService rs = new ReviewService();
            game.setReviews(rs.getReviewsOfGame(game.getId()));
            games.add(game);
        }
        return games;
    }

    public List<Game> getGamesByName(Class<?> clazz, String nameSubstring){
        List<Game> games = new ArrayList<>();
        for(Object ob: OrmImpl.getAll(Game.class)){
            Game game = (Game)ob;
            ReviewService rs = new ReviewService();
            game.setReviews(rs.getReviewsOfGame(game.getId()));
            games.add(game);
        }
        return games.stream()
                .filter(g->nameSubstring.equals(g.getGameName()))
                .collect(Collectors.toList());
    }

    public boolean createGame(Game game) {
        Object ob = OrmImpl.create(game);
        if (ob != null) {
            return true;
        }
        return false;
    }

    public boolean deleteGame(int id){
      //  OrmImpl.delete(Review.class, id, "game_id");
        return OrmImpl.delete(Game.class, id, "game_id");
    }

}
