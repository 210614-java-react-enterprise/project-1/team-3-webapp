package dev.team3.webapp.services;

//import dev.team3.orm.orm.OrmImpl;
import dev.team3.orm.orm.OrmImpl;
import dev.team3.webapp.models.Game;
import dev.team3.webapp.models.Review;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class ReviewService {

    private OrmImpl orm = new OrmImpl();

    public List<Review> getReviewsOfGame(int id){
        List<Review> reviews = new ArrayList<>();
        for(Object ob: orm.getAll(Review.class)){
            reviews.add((Review)ob);
        }
        return   reviews.stream()
                .filter(r->id == r.getGameId())
                .collect(Collectors.toList());
    }

    public List<Review> getReviewsOfUser(int id){
         List<Review> reviews = new ArrayList<>();
         for(Object ob: orm.getAll(Review.class)){
             reviews.add((Review)ob);
         }
        return   reviews.stream()
                .filter(r->id == r.getUserId())
                .collect(Collectors.toList());
    }

    public Review getOneReviewOfUser(int user_id, int game_id){
         List<Review> reviews = new ArrayList<>();
         for(Object ob: orm.getAll(Review.class)){
             reviews.add((Review)ob);
         }
         Review review = null;
         review  = reviews.stream()
                 .filter(r->user_id == r.getUserId())
                 .filter(r->game_id == r.getGameId())
                 .findFirst().orElse(null);

         return review;
    }

    public boolean createReview(Review review){
        if(OrmImpl.create(review) != null){
            return true;
        }else{
            return true;
        }
    }

    public boolean updateReview(Review review){
        OrmImpl.update(review);
        return true;
    }

    public boolean deleteReview(int id, String column){
        return OrmImpl.delete(Review.class, id, column);
    }
}
