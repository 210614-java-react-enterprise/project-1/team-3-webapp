package dev.team3.webapp.services;

import dev.team3.orm.orm.OrmImpl;
import dev.team3.webapp.models.User;

public class AuthService {

    private OrmImpl orm = new OrmImpl();

    public User getPermissions(String user, String password) {
        User userAuth = new User();
        userAuth.setUsername(user);
        userAuth.setPassword(password);
        int success = orm.authenticateUser(user, password);
        if (success == 2) {
            System.out.println("Logged in as administrator.");
            userAuth.setAdmin(true);
            return userAuth;
        } else if (success == 1) {
            System.out.println("Logged in as user.");
            return userAuth;
        } else {
            System.out.println("Invalid credentials.");
            return null;
        }
    }
}
