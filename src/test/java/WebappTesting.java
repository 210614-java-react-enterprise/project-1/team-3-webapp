import dev.team3.orm.util.ConnectionUtil;
import dev.team3.webapp.models.Game;
import dev.team3.webapp.models.Review;
import dev.team3.webapp.models.User;
import dev.team3.webapp.services.*;
import org.h2.tools.RunScript;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class WebappTesting {

    AuthService as = new AuthService();
    GameService gs = new GameService();
    ReviewService rs = new ReviewService();
    UserService us = new UserService();

    @BeforeAll
    public static void runSetup() throws SQLException, FileNotFoundException {
        Connection connection = ConnectionUtil.getConnection();
        RunScript.execute(connection, new FileReader("src/setup.sql"));
    }

    @Test
    public void getAllGamesTestNotNull(){
        assertNotNull(gs.getAllGames(Game.class));
    }


    @Test
    public void getAllGamesTestNumberOfRecords(){
        assertEquals(2, gs.getAllGames(Game.class).size());
    }

    @Test
    public void getAllGamesTestSubstringSearch(){
        assertEquals(1, gs.getGamesByName(Game.class, "Battlefield").size());
    }

    @Test
    public void testSuccessfulCreation(){
        assertNotNull(gs.createGame(new Game("Test game")));
    }

    @Test
    public void testingIsAdmin(){
        User user = as.getPermissions("test1", "pass1");
        assertTrue(user.isAdmin());
    }

    @Test
    public void testingIsAdminFalse(){
        User user = as.getPermissions("test2", "pass2");
        assertFalse(user.isAdmin());
    }

    @Test
    public void testingIsUserTrue(){
        User user = as.getPermissions("test2", "pass2");
        assertTrue(user!=null);
    }

    @Test
    public void testingIsUserFalse(){
        User user = as.getPermissions("test6", "pass6");
        assertTrue(user==null);
    }

    @Test
    public void retrieveReviewsTest(){
        List<Review> rev = rs.getReviewsOfGame(1);
        assertEquals(1, rev.size());
    }

    @Test
    public void retrieveSpecificReview(){
        List<Review> rev = new ArrayList<>();
        Review review = rs.getOneReviewOfUser(1, 1);
        rev.add(review);
        assertEquals(1,rev.size());
    }


    @Test
    public void retrieveReviewsUser(){
        List<Review> rev = rs.getReviewsOfUser(1);
        assertEquals(2, rev.size());
    }

    @Test
    public void SuccessfulCreateReview(){
        Review rev = new Review();
        rev.setReview("Hello!!");
        rev.setGameId(2);
        rev.setUserId(2);
        assertTrue(rs.createReview(rev));
    }

    @Test
    public void testSuccessfulCreateUser(){
        User user = new User();
        user.setUsername("test3");
        user.setPassword("pass3");
        assertNotNull(us.createUser(user));
    }

//    @Test
//    public void testSuccessfulDeleteUser(){
//        assertTrue(us.deleteUser(2));
//    }

    @Test
    public void testSuccessfulGetUser(){
        assertNotNull(us.getUser(2));
    }

    @Test
    public void testSuccessfulUpdateUser(){
        assertTrue(us.updateUser("test1", "pass1", 1));
    }

}
