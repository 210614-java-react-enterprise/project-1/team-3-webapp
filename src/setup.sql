create table games(
	game_id serial primary key,
	game_name varchar(100)
);

create table users(
	user_id serial primary key,
	username varchar(50),
	password varchar(50),
	admin boolean
);

create table reviews(
	game_id integer references games,
	user_id integer references users,
	review varchar(2000),
	primary key(game_id, user_id)
);


insert into games values (default, 'Call of Duty');
insert into games values (default, 'Battlefield');

insert into users values (default, 'test1', 'pass1', true);
insert into users values (default, 'test2', 'pass2', false);

insert into reviews values (1, 1, 'This game is great!');
insert into reviews values (2, 1, 'This game is awful!');


